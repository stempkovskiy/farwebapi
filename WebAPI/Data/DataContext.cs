﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebAPI.Domain;

namespace WebAPI.Data
{
    public class DataContext: IdentityDbContext
    {
        public DbSet<UserInfo> UserInfos { get; set; } 
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Restauran> Restaurans { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<FilePath> FilePaths { get; set; }
        public DbSet<Followers> Followers { get; set; }
        public DbSet<Following> Followings { get; set; }
        public DbSet<TimeWork> TimeWorks { get; set; } 
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}
    }
}

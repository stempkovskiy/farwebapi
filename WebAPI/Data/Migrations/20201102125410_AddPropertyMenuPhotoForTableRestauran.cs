﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Data.Migrations
{
    public partial class AddPropertyMenuPhotoForTableRestauran : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RestauranId1",
                table: "FilePaths",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FilePaths_RestauranId1",
                table: "FilePaths",
                column: "RestauranId1");

            migrationBuilder.AddForeignKey(
                name: "FK_FilePaths_Restaurans_RestauranId1",
                table: "FilePaths",
                column: "RestauranId1",
                principalTable: "Restaurans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FilePaths_Restaurans_RestauranId1",
                table: "FilePaths");

            migrationBuilder.DropIndex(
                name: "IX_FilePaths_RestauranId1",
                table: "FilePaths");

            migrationBuilder.DropColumn(
                name: "RestauranId1",
                table: "FilePaths");
        }
    }
}

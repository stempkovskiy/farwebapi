﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Data.Migrations
{
    public partial class ChangePropertyUserInfoAndAddPropertyInFilePath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserInfos_AspNetUsers_IdentityUserId",
                table: "UserInfos");

            migrationBuilder.DropIndex(
                name: "IX_UserInfos_IdentityUserId",
                table: "UserInfos");

            migrationBuilder.DropColumn(
                name: "Avatar",
                table: "UserInfos");

            migrationBuilder.DropColumn(
                name: "IdentityUserId",
                table: "UserInfos");

            migrationBuilder.AddColumn<string>(
                name: "AvatarId",
                table: "UserInfos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "FilePaths",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserInfos_AvatarId",
                table: "UserInfos",
                column: "AvatarId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserInfos_FilePaths_AvatarId",
                table: "UserInfos",
                column: "AvatarId",
                principalTable: "FilePaths",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserInfos_FilePaths_AvatarId",
                table: "UserInfos");

            migrationBuilder.DropIndex(
                name: "IX_UserInfos_AvatarId",
                table: "UserInfos");

            migrationBuilder.DropColumn(
                name: "AvatarId",
                table: "UserInfos");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "FilePaths");

            migrationBuilder.AddColumn<string>(
                name: "Avatar",
                table: "UserInfos",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IdentityUserId",
                table: "UserInfos",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserInfos_IdentityUserId",
                table: "UserInfos",
                column: "IdentityUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserInfos_AspNetUsers_IdentityUserId",
                table: "UserInfos",
                column: "IdentityUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

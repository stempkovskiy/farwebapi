﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Data.Migrations
{
    public partial class AddRestauranAndUsersReviewTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Restaurans",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Kitchen = table.Column<string>(nullable: true),
                    KM = table.Column<double>(nullable: false),
                    Street = table.Column<string>(nullable: true),
                    Rating = table.Column<double>(nullable: false),
                    Photos = table.Column<byte[]>(nullable: true),
                    BeginWork = table.Column<string>(nullable: true),
                    EndWork = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restaurans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UsersReview",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Avatar = table.Column<byte[]>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Review = table.Column<string>(nullable: true),
                    Rating = table.Column<double>(nullable: false),
                    UserInfoId = table.Column<string>(nullable: true),
                    RestauranId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersReview", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsersReview_Restaurans_RestauranId",
                        column: x => x.RestauranId,
                        principalTable: "Restaurans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UsersReview_UserInfos_UserInfoId",
                        column: x => x.UserInfoId,
                        principalTable: "UserInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UsersReview_RestauranId",
                table: "UsersReview",
                column: "RestauranId");

            migrationBuilder.CreateIndex(
                name: "IX_UsersReview_UserInfoId",
                table: "UsersReview",
                column: "UserInfoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UsersReview");

            migrationBuilder.DropTable(
                name: "Restaurans");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Data.Migrations
{
    public partial class AddDbSetTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Following_UserInfos_UserInfoId",
                table: "Following");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Following",
                table: "Following");

            migrationBuilder.RenameTable(
                name: "Following",
                newName: "Followings");

            migrationBuilder.RenameIndex(
                name: "IX_Following_UserInfoId",
                table: "Followings",
                newName: "IX_Followings_UserInfoId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Followings",
                table: "Followings",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Followings_UserInfos_UserInfoId",
                table: "Followings",
                column: "UserInfoId",
                principalTable: "UserInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Followings_UserInfos_UserInfoId",
                table: "Followings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Followings",
                table: "Followings");

            migrationBuilder.RenameTable(
                name: "Followings",
                newName: "Following");

            migrationBuilder.RenameIndex(
                name: "IX_Followings_UserInfoId",
                table: "Following",
                newName: "IX_Following_UserInfoId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Following",
                table: "Following",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Following_UserInfos_UserInfoId",
                table: "Following",
                column: "UserInfoId",
                principalTable: "UserInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

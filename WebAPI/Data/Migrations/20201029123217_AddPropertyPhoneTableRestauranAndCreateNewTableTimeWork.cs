﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Data.Migrations
{
    public partial class AddPropertyPhoneTableRestauranAndCreateNewTableTimeWork : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BeginWork",
                table: "Restaurans");

            migrationBuilder.DropColumn(
                name: "EndWork",
                table: "Restaurans");

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Restaurans",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TimeWorks",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Day = table.Column<string>(nullable: true),
                    BeginWork = table.Column<string>(nullable: true),
                    EndWork = table.Column<string>(nullable: true),
                    RestauranId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeWorks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TimeWorks_Restaurans_RestauranId",
                        column: x => x.RestauranId,
                        principalTable: "Restaurans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TimeWorks_RestauranId",
                table: "TimeWorks",
                column: "RestauranId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TimeWorks");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Restaurans");

            migrationBuilder.AddColumn<string>(
                name: "BeginWork",
                table: "Restaurans",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EndWork",
                table: "Restaurans",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

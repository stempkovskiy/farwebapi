﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Data.Migrations
{
    public partial class Edit_UserReviwe_UserInfo_And_RestauranTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Avatar",
                table: "UsersReview");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "UsersReview");

            migrationBuilder.DropColumn(
                name: "Reviews",
                table: "UserInfos");

            migrationBuilder.AddColumn<int>(
                name: "CountReview",
                table: "UserInfos",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Login",
                table: "UserInfos",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Photos",
                table: "Restaurans",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "varbinary(max)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CountReview",
                table: "UserInfos");

            migrationBuilder.DropColumn(
                name: "Login",
                table: "UserInfos");

            migrationBuilder.AddColumn<byte[]>(
                name: "Avatar",
                table: "UsersReview",
                type: "varbinary(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "UsersReview",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Reviews",
                table: "UserInfos",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<byte[]>(
                name: "Photos",
                table: "Restaurans",
                type: "varbinary(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

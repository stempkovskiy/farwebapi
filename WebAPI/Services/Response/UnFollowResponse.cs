﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Domain;

namespace WebAPI.Services.Response
{
    public class UnFollowResponse
    {
        public bool Success { get; set; }
        public List<Following> Followings { get; set; }
    }
}

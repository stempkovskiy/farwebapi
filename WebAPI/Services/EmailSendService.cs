﻿using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;


namespace WebAPI.Services
{
    public class EmailSendService
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Site administration Food&Reviews", "FoodAndReview@yandex.by"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;

            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };


            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.yandex.by", 25, false);
                await client.AuthenticateAsync("FoodAndReview@yandex.by", "!FoodAndReview");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }

        }
    }
}

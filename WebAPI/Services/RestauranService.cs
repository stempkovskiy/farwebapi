﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Data;
using WebAPI.Domain;
using WebAPI.Utils;

namespace WebAPI.Services
{
    public class RestauranService : IRestauranService
    {
        private readonly DataContext _context;
        public RestauranService(DataContext context)
        {
            _context = context;
        }
        public async Task<bool> AddRestauranAsync(RestauranInfo request)
        {
            var restauran = await _context.Restaurans
                .FirstOrDefaultAsync(r => r.Name == request.Name);
            if (restauran == null)
            {
                var newRestaurant = new Restauran()
                {
                    Phone = request.Phone,
                    Kitchen = request.Kitchen,
                    TimeWorks = request.TimeWorks,
                    KM = request.KM,
                    Photos = new List<FilePath>(),
                    MenuPhoto = new List<FilePath>(),
                    Name = request.Name,
                    Street = request.Street
                };

                newRestaurant.Photos = GetFilePathsAsync(request.Photos, ImageFolder.Restauran);
                newRestaurant.MenuPhoto = GetFilePathsAsync(request.MenuPhotos, ImageFolder.Menu);

                await _context.Restaurans.AddAsync(newRestaurant);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

       
        public async Task<bool> EditInfoRestaurantAsync(RestauranInfo request)
        {
            var restauran = await GetRestaurantByIdAsync(request.Id);
            if (restauran != null)
            {
                restauran.Kitchen = request.Kitchen ?? restauran.Kitchen;
                restauran.Name = request.Name ?? restauran.Name;
                restauran.Phone = request.Phone ?? restauran.Phone;
                restauran.Street = request.Street ?? restauran.Street;
                restauran.TimeWorks = request.TimeWorks ?? restauran.TimeWorks;

                restauran.Photos = GetFilePathsAsync(request.Photos, ImageFolder.Restauran);
                restauran.MenuPhoto = GetFilePathsAsync(request.MenuPhotos, ImageFolder.Menu);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> RemoveRestauranAsync(string id)
        {
            var restauran = await GetRestaurantByIdAsync(id);
            if (restauran != null)
            {
                _context.Restaurans.Remove(restauran);
                var changes = await _context.SaveChangesAsync();
                return changes > 0;
            }
            return false;
        }


        public async Task<Restauran> FindRestaurantAsync(string id) =>
             await GetRestaurantByIdAsync(id);

        public async Task<List<Restauran>> GetAllRestaurantsAsync() =>
            await _context.Restaurans
            .Include(r => r.Photos)
            .Include(r => r.TimeWorks)
            .Include(r => r.MenuPhoto)
            .Include(r => r.Reviews)
            .Select(r => r).ToListAsync();

        public async Task<bool> DeleteImagesRstauranAsync(string id)
        {
            var filePath = await _context.FilePaths.FirstOrDefaultAsync(path => path.Id.Equals(id));
            if (filePath != null)
            {
                _context.FilePaths.Remove(filePath);
                await _context.SaveChangesAsync();
                ImageWorker.DeleteImageFromServer(filePath.Path);
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteTimeWorkAsync(string id)
        {
            var time = await _context.TimeWorks
                .FirstOrDefaultAsync(time => time.Id.Equals(id));
            if (time != null)
            {
                _context.TimeWorks.Remove(time);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        private List<FilePath> GetFilePathsAsync(List<IFormFile> imgs, ImageFolder folder)
        {
            var filePath = new List<FilePath>();
            if (imgs != null)
            {
                imgs.ForEach(photo =>
                {
                    string name = ImageWorker.GetImagePath(photo, folder);
                    string url = ImageWorker.UploadImageAzureBlobStorageAsync(name, photo).Result;
                    switch (folder)
                    {
                        case ImageFolder.Restauran:
                            filePath.Add(new FilePath
                            {
                                Path = url,
                                Name = name
                            });
                            break;
                        case ImageFolder.Menu:
                            filePath.Add(new FilePath
                            {
                                Path = url,
                                Name = name
                            });
                            break;
                    }
                });
            }
            return filePath;
        }


        private async Task<Restauran> GetRestaurantByIdAsync(string id) =>
            await _context.Restaurans
            .Include(r => r.Photos)
            .Include(r => r.MenuPhoto)
            .Include(r => r.TimeWorks)
            .Include(r => r.Reviews)
            .FirstOrDefaultAsync(r => r.Id.Equals(id));
    }
}

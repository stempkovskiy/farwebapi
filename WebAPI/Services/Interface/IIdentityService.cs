﻿using System.Threading.Tasks;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Domain;

namespace WebAPI.Services
{
    public interface IIdentityService
    {
        Task<AunthenticationResult> RegisterAsync(UserRegistrationRequest user);
        Task<AunthenticationResult> LoginAsync(string email, string password);
        Task<AunthenticationResult> RefreshTokenAsync(string token, string refreshToken);
    }
}

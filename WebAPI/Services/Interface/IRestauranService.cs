﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Domain;
namespace WebAPI.Services
{
    public interface IRestauranService
    {
        Task<bool> AddRestauranAsync(RestauranInfo restauran);
        Task<List<Restauran>> GetAllRestaurantsAsync();
        Task<bool> RemoveRestauranAsync(string id);
        Task<Restauran> FindRestaurantAsync(string id);
        Task<bool> EditInfoRestaurantAsync(RestauranInfo id);
        Task<bool> DeleteImagesRstauranAsync(string id);
        Task<bool> DeleteTimeWorkAsync(string id);
    }
}

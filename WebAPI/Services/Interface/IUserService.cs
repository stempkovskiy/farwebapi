﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Contracts.V1.Requests.UserRequests;
using WebAPI.Domain;
using WebAPI.Services.Response;

namespace WebAPI.Services
{
    public interface IUserService
    {
        Task<List<UserInfo>> GetAllUsersAsync();
        Task<UserInfo> GetUsersAsync(string id);
        Task<UserInfo> GetUserByEmail(string email);
        Task<bool> FollowAsync(string idFollow, string idUser);
        Task<UnFollowResponse> UnFollowAsync(string idUnFollow, string idUser);
        Task<UnFollowResponse> GetAllNonFollowUsers(string idUser);
        Task<bool> LoadAvatarAsync(string id, IFormFile avatar);
        Task<bool> AddReviewAsync(ReviewRequest request);
        Task<bool> DeleteReviewAsync(IdRequest userId);
        Task<bool> DeleteUserAsync(string id);
    }
}

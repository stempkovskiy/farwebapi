﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Validations;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Utilities.Collections;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Contracts.V1.Requests.UserRequests;
using WebAPI.Data;
using WebAPI.Domain;
using WebAPI.Services.Response;
using WebAPI.Utils;

namespace WebAPI.Services
{
    public class UserService : IUserService
    {
        private readonly DataContext _context;
        public UserService(DataContext context, IHostEnvironment host)
        {
            _context = context;
        }
        public async Task<bool> FollowAsync(string idFollow, string idUser)
        {
            var currentUser = await GetUsersAsync(idUser);
            var following = await GetUsersAsync(idFollow);
            if (following != null && currentUser != null)
            {
                if (await AddFollowingTableAsync(currentUser.Following, following))
                {
                    if (await AddFollowerTableAsync(following.Followers, currentUser))
                    {
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        private async Task<bool> AddFollowerTableAsync(List<Followers> followers, UserInfo curretUser)
        {
            if (followers.FirstOrDefault(f => f.IdFollow.Equals(curretUser.Id)) == null)
            {
                var reviews = _context.Reviews.Select(r => r.UserInfoId.Equals(curretUser.Id));
                followers.Add(new Followers
                {
                    Avatar = curretUser.Avatar.Path,
                    CountReview = reviews.Count(),
                    Login = curretUser.Login,
                    IdFollow = curretUser.Id
                });
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        private async Task<bool> AddFollowingTableAsync(List<Following> followings, UserInfo follow)
        {
            if (followings.FirstOrDefault(f => f.IdFollowing.Equals(follow.Id)) == null)
            {
                var reviews = _context.Reviews.Select(r => r.UserInfoId.Equals(follow.Id));
                followings.Add(new Following
                {
                    Avatar = follow.Avatar.Path,
                    CountReview = reviews.Count(),
                    Login = follow.Login,
                    IdFollowing = follow.Id
                });
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<UnFollowResponse> UnFollowAsync(string idUnFollow, string idUser)
        {
            await _context.Followings.LoadAsync();
            await _context.Followers.LoadAsync();
            await _context.Followings.ForEachAsync(f => {
                if (f.Id.Equals(idUnFollow))
                {
                    _context.Followings.Remove(f);
                }
            });
            await _context.Followers.ForEachAsync(f =>
            {
                if (f.IdFollow.Equals(idUser))
                {
                    _context.Followers.Remove(f);
                }
            });
            var result = await _context.SaveChangesAsync();
            if (result > 0)
            {
                return new UnFollowResponse
                {
                    Success = true,
                    Followings = _context.Followings.Where(f => f.UserInfoId.Equals(idUser)).ToList()
                };
            }
            return new UnFollowResponse
            {
                Success = false
            };
        }

        public async Task<bool> LoadAvatarAsync(string id, IFormFile avatar)
        {
            var user = await _context.UserInfos.FirstOrDefaultAsync(u => u.Id.Equals(id));
            if (user != null)
            {
                string path = ImageWorker.GetImagePath(avatar, ImageFolder.Avatar);
                string uri = await ImageWorker.UploadImageAzureBlobStorageAsync(path, avatar);
                user.Avatar = new FilePath
                {
                    Name = path,
                    Path = uri
                };
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> AddReviewAsync(ReviewRequest request)
        {
            var user = await GetUsersAsync(request.IdUser);
            if (user != null)
            {
                _context.Reviews.Load();
                var reviews = _context.Reviews.Where(r => r.Id.Equals(request.IdRestauran)).ToList();
                var restauran = await _context.Restaurans
                    .Include(r => r.Photos)
                    .FirstOrDefaultAsync(r => r.Id.Equals(request.IdRestauran));

                var review = new Review
                {
                    Avatar = user.Avatar.Path,
                    Name = user.Login ?? "",
                    Resturan = restauran.Name ?? "",
                    RestauranPhoto = restauran.Photos[0].Path ?? "",
                    Rating = reviews.Count != 0
                    ? CulcRating(reviews.Select(r => r.Rating).ToList())
                    : CulcRating(new List<double> { request.Rating }),
                    Text = request.Text,
                    RestauranId = request.IdRestauran,
                    UserInfoId = request.IdUser
                };
                restauran.Rating = review.Rating;
                _context.Reviews.Add(review);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        private double CulcRating(List<double> ratings)
            => Math.Round(ratings.Sum() / ratings.Count, 1);

        public async Task<bool> DeleteReviewAsync(IdRequest userId)
        {
            await _context.Reviews.LoadAsync();
            string id = "";
            await _context.Reviews.ForEachAsync(review => {
                if (review.Id.Equals(userId.Id))
                {
                    id = review.RestauranId;
                    _context.Reviews.Remove(review);
                }
            });
            var restauran = await _context.Restaurans.FirstOrDefaultAsync(r => r.Id.Equals(id));
            var restauranReviews = await _context.Reviews.Where(r => r.RestauranId.Equals(id)).ToListAsync();
            restauran.Rating = CulcRating(restauranReviews.Select(r => r.Rating).ToList());
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteUserAsync(string email)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email.Equals(email));
            var refreshToken = await _context.RefreshTokens.FirstOrDefaultAsync(r => r.UserId.Equals(user.Id));
            _context.RefreshTokens.Remove(refreshToken);
            var userInfo = await _context.UserInfos
                .Include(u => u.Following)
                .Include(u => u.Followers)
                .Include(u => u.Avatar)
                .FirstOrDefaultAsync(u => u.UserId.Equals(user.Id));

            _context.Reviews.Load();
            _context.Followers.Load();

            if (userInfo != null)
            {
                _context.UserInfos.Remove(userInfo);

                await _context.Followers
                    .ForEachAsync(f =>
                    {
                        if (f.IdFollow != null)
                        {
                            if (f.IdFollow.Equals(userInfo.Id))
                            {
                                _context.Followers.Remove(f);
                            }
                        }
                    });

                await _context.Reviews
                    .ForEachAsync(rr =>
                    {
                        if (rr.UserInfoId != null)
                        {
                            if (rr.UserInfoId.Equals(userInfo.Id))
                            {
                                _context.Reviews.Remove(rr);
                            }
                        }
                    });
                await ImageWorker.DeleteImageAzureBlobStoragAsync(userInfo.Avatar.Name);
                return true;
            }
            return false;
        }

        public async Task<List<UserInfo>> GetAllUsersAsync()
        {
            return await _context.UserInfos
                .Include(u => u.Reviews)
                .Include(u => u.Avatar)
                .Include(u => u.Followers)
                .Include(u => u.Following)
                .Select(u => u).ToListAsync();
        }

        public async Task<UserInfo> GetUsersAsync(string id)
        {
            return await _context.UserInfos
                .Include(u => u.Avatar)
                .Include(u => u.Reviews)
                .Include(u => u.Followers)
                .Include(u => u.Following)
                .FirstOrDefaultAsync(u => u.Id.Equals(id));
        }

        public async Task<UnFollowResponse> GetAllNonFollowUsers(string idUser)
        {
            var users = await _context.UserInfos
                .Include(u => u.Followers)
                .Include(u => u.Avatar)
                .Include(u => u.Reviews)
                .ToArrayAsync();
            var unFollowUsers = users.Where(user =>
            {
                if (user.Followers != null && !user.Id.Equals(idUser))
                {
                    var isUserFolloing = user.Followers.FirstOrDefault(f => f.IdFollow.Equals(idUser));
                    if (isUserFolloing == null)
                    {
                        return true;
                    }
                }
                return false;
            }).ToList();

            var followings = new List<Following>();
            int id = 0;
            unFollowUsers.ForEach(unFollow =>
            {
                followings.Add(new Following
                {
                    Id = $"{id++}",
                    Avatar = unFollow.Avatar.Path,
                    CountReview = unFollow.Reviews.Count,
                    Login = unFollow.Login,
                    IdFollowing = unFollow.Id,
                    UserInfoId = idUser
                });
            });
            if (unFollowUsers != null)
            {
                return new UnFollowResponse
                {
                    Success = true,
                    Followings = followings
                };
            }
            return new UnFollowResponse
            {
                Success = false
            };
        }

        public async Task<UserInfo> GetUserByEmail(string email)
            => await _context.UserInfos
                 .Include(u => u.Avatar)
                 .Include(u => u.Reviews)
                 .Include(u => u.Followers)
                 .Include(u => u.Following)
                 .FirstOrDefaultAsync(u => u.Email.Equals(email));
    }
}

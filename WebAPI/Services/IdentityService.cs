﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Data;
using WebAPI.Domain;
using WebAPI.Options;
using WebAPI.Utils;

namespace WebAPI.Services
{
    public class IdentityService : IIdentityService
    {

        private readonly UserManager<IdentityUser> _userManager;
        private readonly JwtSettings _jwtSettings;
        private readonly TokenValidationParameters _tokenValidationParameters;
        private readonly DataContext _context;
        private readonly IHostEnvironment _hostEnv;

        public IdentityService(UserManager<IdentityUser> userManager, JwtSettings jwtSettings, 
            TokenValidationParameters tokenValidationParameters, DataContext context, IHostEnvironment hostEnv)
        {
            _userManager = userManager;
            _jwtSettings = jwtSettings;
            _tokenValidationParameters = tokenValidationParameters;
            _context = context;
            _hostEnv = hostEnv;
        }


        public async Task<AunthenticationResult> RegisterAsync(UserRegistrationRequest user)
        {
            var existingUser = await _userManager.FindByEmailAsync(user.Email);

            if (existingUser != null)
            {
                return new AunthenticationResult
                {
                    Errors = new[] { "User with this email adress alredy exists" }
                };
            }

            var newUser = new IdentityUser()
            {
                Email = user.Email,
                UserName = user.Name
            };

            var createdUser = await _userManager.CreateAsync(newUser, user.Password);

            if (!createdUser.Succeeded)
            {
                return new AunthenticationResult
                {
                    Errors = createdUser.Errors.Select(error => error.Description)
                };
            }

            if (user.Avatar != null)
            {
                string path = ImageWorker.GetImagePath(user.Avatar, ImageFolder.Avatar);
                string uri = await ImageWorker.UploadImageAzureBlobStorageAsync(path, user.Avatar);
                UserInfo info = new UserInfo
                {
                    Login = newUser.UserName,
                    Avatar = new FilePath {
                        Name = path,
                        Path = uri
                    },
                    Email = newUser.Email,
                    UserId = (await _context.Users.FirstOrDefaultAsync(u => u.Email == newUser.Email)).Id
                };
                await _context.UserInfos.AddAsync(info);
                await _context.SaveChangesAsync();
            }

            return await GenerateAuthenticationResultForUser(newUser);
        }

        public async Task<AunthenticationResult> LoginAsync(string email, string password)
        {
            var user = await _userManager.FindByEmailAsync(email);

            if (user == null)
            {
                return new AunthenticationResult
                {
                    Errors = new[] { "User does not exist" }
                };
            }

            var userHasValidPassword = await _userManager.CheckPasswordAsync(user, password);

            if (!userHasValidPassword)
            {
                return new AunthenticationResult
                {
                    Errors = new[] { "User/password combination is wrong" }
                };
            }

            return await GenerateAuthenticationResultForUser(user);
        }

        private async Task<AunthenticationResult> GenerateAuthenticationResultForUser(IdentityUser newUser) 
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                        new Claim(JwtRegisteredClaimNames.Sub, newUser.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Email, newUser.Email),
                        new Claim("id", newUser.Id),
                    }),
                Expires = DateTime.UtcNow.Add(_jwtSettings.TokenTimeLife),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var refreshToken = new RefreshToken
            {
                JwtId = token.Id,
                UserId = newUser.Id,
                CreationDate = DateTime.UtcNow,
                ExpiryDate = DateTime.UtcNow.AddMonths(6)
            };

            await _context.RefreshTokens.AddAsync(refreshToken);
            await _context.SaveChangesAsync();

            return new AunthenticationResult
            {
                Success = true,
                Token = tokenHandler.WriteToken(token),
                RefreshToken = refreshToken.Token,
                UserInfo = await _context.UserInfos
                .Include(u => u.Reviews)
                .Include(u => u.Avatar)
                .Include(u => u.Followers)
                .Include(u => u.Following)
                .FirstOrDefaultAsync(u => u.UserId.Equals(newUser.Id))
            };
        }

        public async Task<AunthenticationResult> RefreshTokenAsync(string token, string refreshToken)
        {
            var validationToken = GetPrincipalFromToken(token);
            if (validationToken == null)
            {
                return new AunthenticationResult { Errors = new[] { "Invalid Token" } };
            }

            var expiryDateUnix =
                long.Parse(validationToken.Claims.Single(claim => claim.Type == JwtRegisteredClaimNames.Exp).Value);

            var expiryDateTimeUtc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                .AddSeconds(expiryDateUnix);

            if (expiryDateTimeUtc > DateTime.UtcNow)
            {
                return new AunthenticationResult { Errors = new[] { "This token hasn't expired yet" } };
            }

            var jti = validationToken.Claims.Single(claim => claim.Type == JwtRegisteredClaimNames.Jti).Value;

            var storedRefresToken = await _context.RefreshTokens.SingleOrDefaultAsync(refToken => refToken.Token == refreshToken);

            if (storedRefresToken == null )
            {
                return new AunthenticationResult { Errors = new[] { "This refresh token does not exist" } };
            }

            if (DateTime.UtcNow > storedRefresToken.ExpiryDate)
            {
                return new AunthenticationResult { Errors = new[] { "This refresh token has expared" } };
            }

            if (storedRefresToken.Invalidated)
            {
                return new AunthenticationResult { Errors = new[] { "This refresh token has been invalidated" } };
            }

            if (storedRefresToken.Used)
            {
                return new AunthenticationResult { Errors = new[] { "This refresh token has been used" } };
            }

            if (storedRefresToken.JwtId != jti)
            {
                return new AunthenticationResult { Errors = new[] { "This refresh token does not match this JWT" } };
            }

            storedRefresToken.Used = true;
            _context.RefreshTokens.Update(storedRefresToken);
            await _context.SaveChangesAsync();

            var user = await _userManager.FindByIdAsync(validationToken.Claims.Single(claim => claim.Type == "id").Value);

            return await GenerateAuthenticationResultForUser(user);
        }

        private ClaimsPrincipal GetPrincipalFromToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                var principal = tokenHandler.ValidateToken(token, _tokenValidationParameters, out var validationToken);
                if (!IsJwtWithValidSecurityAlgorithm(validationToken))
                {
                    return null;
                }
                return principal;
            }
            catch (Exception e)
            {
                Debug.Write(e); 
                return null;
            }
        }

        private bool IsJwtWithValidSecurityAlgorithm(SecurityToken validatedToken)
        {
            return (validatedToken is JwtSecurityToken jwtSecurityToken) &&
                jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, 
                StringComparison.InvariantCultureIgnoreCase);
        }

    }
}

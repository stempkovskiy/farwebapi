﻿

using System.Collections.Generic;

namespace WebAPI.Contracts.V1.Responses
{
    public class BaseResponse
    {
        public bool Success { get; set; }
        public List<string> Message { get; set; }

    }
}

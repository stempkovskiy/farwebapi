﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using WebAPI.Domain;

namespace WebAPI.Contracts.V1.Responses
{
    public class UserInfoResponse
    {
        public bool Success { get; set; }
        public List<UserInfo> UserInfos {get; set;}
    }
}

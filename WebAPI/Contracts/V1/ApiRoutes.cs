﻿namespace WebAPI.Contracts
{
    public static class ApiRoutes
    {
        public const string Root = "api";
        public const string Version = "v1";
        public const string Base = Root + "/" +  Version;
        public static class Identity
        {
            public const string Login = Base + "/identity/login";
            public const string Register = Base + "/identity/register";
            public const string Refresh = Base + "/identity/refresh";
            public const string ForgotPassword = Base + "/identity/forgotPassword";
            public const string ResetPassword = Base + "/identity/resetPassword";
        }

        public static class Restaurants
        {
            public const string GetAllRestaurants = Base + "/restaurants/getAllRestaurants";
            public const string AddRestauran = Base + "/restaurants/addRestauran";
            public const string EditInfoRestauran = Base + "/restaurants/editInfoRestauran";
            public const string FindRestauran = Base + "/restaurants/findRestauran";
            public const string DeleteRestauran = Base + "/restaurants/removeRestauran";
            public const string DeleteImagesRestauran = Base + "/restaurants/deleteImagesRestauran";
            public const string DeleteTimeWork = Base + "/restaurants/deleteTimeWork";
        }

        public static class User
        {
            public const string GetAllUsers = Base + "/user/getAllUsers";
            public const string GetUser = Base + "/user/getUser";
            public const string GetUserByEmail = Base + "/user/getUserByEmail";
            public const string GetUserReviews = Base + "/user/getUserReviews"; //TODO
            public const string GetAllNonFollowUsers = Base + "/user/getAllNonFollowUsers"; 
            public const string Follow = Base + "/user/follow";
            public const string UnFollow = Base + "/user/unfollow";
            public const string AddReview = Base + "/user/addReview";
            public const string DeleteReview = Base + "/user/deleteReview";
            public const string DeleteUser = Base + "/user/deleteUser";
            public const string EditReview = Base + "/user/editReview"; //TODO
            public const string LoadAvatar = Base + "/user/loadAvatar";
        }

        public static class UserInfo
        {
            public const string GetInfo = Base + "/user/getInfo";
        }
    }
}

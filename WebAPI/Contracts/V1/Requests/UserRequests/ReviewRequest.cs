﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Contracts.V1.Requests.UserRequests
{
    public class ReviewRequest
    {
        public string IdUser { get; set; }
        public string IdRestauran { get; set; }
        public string Text { get; set; }
        public double Rating { get; set; }

    }
}

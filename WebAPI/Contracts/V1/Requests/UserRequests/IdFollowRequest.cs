﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Contracts.V1.Requests
{
    public class IdFollowRequest
    {
        public string IdFollow { get; set; }
        public string IdUser { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Contracts.V1.Requests.UserRequests
{
    public class LoadAvatarRequest
    {
        public string Id { get; set; }
        public IFormFile Avatar { get; set; }
    }
}

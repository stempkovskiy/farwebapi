﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Contracts.V1.Requests.UserRequests
{
    public class DeleteUserRequest
    {
        public string Email { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using WebAPI.Domain;

namespace WebAPI.Contracts.V1.Requests
{
    public class RestauranInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Kitchen { get; set; }
        public double KM { get; set; }
        public string Street { get; set; }
        public double Rating { get; set; }
        public List<IFormFile> Photos { get; set; }
        public List<IFormFile> MenuPhotos { get; set; }
        public List<TimeWork> TimeWorks { get; set; }
    }
}

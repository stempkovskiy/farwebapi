﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Contracts.V1.Requests
{
    public class IdRequest
    {

       public string Id { get; set; }
    }
}

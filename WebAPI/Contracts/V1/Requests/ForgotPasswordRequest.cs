﻿using System.ComponentModel.DataAnnotations;


namespace WebAPI.Contracts.V1.Requests
{
    public class ForgotPasswordRequest
    {
        public string Email { get; set; }

    }
}

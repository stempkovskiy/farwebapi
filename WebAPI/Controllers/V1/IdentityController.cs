﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Contracts;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Contracts.V1.Responses;
using WebAPI.Services;

namespace WebAPI.Controllers.V1
{
    [ApiController]
    public class IdentityController : ControllerBase
    {
        private readonly IIdentityService _identityService;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly EmailSendService _emailSendService;
        public IdentityController(IIdentityService identityService, UserManager<IdentityUser> userManager, EmailSendService emailSendService)
        {
            _identityService = identityService;
            _userManager = userManager;
            _emailSendService = emailSendService;
        }

        [HttpPost(ApiRoutes.Identity.Register)]
        public async Task<IActionResult> Register([FromForm]UserRegistrationRequest request )
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(new AuthFailedResponse {
                    Errors = ModelState.Values.SelectMany(value => value.Errors.Select(error => error.ErrorMessage))
                });
            }
            var authResponse = await _identityService.RegisterAsync(request);

            if (!authResponse.Success)
            {
                return BadRequest(new AuthFailedResponse { Errors = authResponse.Errors });
            }

            return Ok(new AuthSuccessResponse {
                Token = authResponse.Token,
                RefreshToken = authResponse.RefreshToken
            });
        }

        [HttpPost(ApiRoutes.Identity.Login)]
        public async Task<IActionResult> Login([FromBody]UserLoginRequest request)
        {
            var authResponse = await _identityService.LoginAsync(request.Email, request.Password);

            if (!authResponse.Success)
            {
                return BadRequest(new AuthFailedResponse { Errors = authResponse.Errors });
            }

            return Ok(new AuthSuccessResponse 
            { 
                Token = authResponse.Token, 
                RefreshToken = authResponse.RefreshToken,
                UserInfo = authResponse.UserInfo
            });
        }

        [HttpPost(ApiRoutes.Identity.Refresh)]
        public async Task<IActionResult> Refresh([FromBody]RefreshTokenRequest request)
        {
            var authResponse = await _identityService.RefreshTokenAsync(request.Token, request.RefreshToken);

            if (!authResponse.Success)
            {
                return BadRequest(new AuthFailedResponse { Errors = authResponse.Errors });
            }

            return Ok(new AuthSuccessResponse
            {
                Token = authResponse.Token,
                RefreshToken = authResponse.RefreshToken
            });
        }

        [HttpPost(ApiRoutes.Identity.ForgotPassword)]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordRequest request)
        {

            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                return BadRequest(new
                {
                    Success = false,
                    ErrorMessage = "User with this Email address is not registered on the site!"
                });
            }

            string code = await _userManager.GeneratePasswordResetTokenAsync(user);
            string codeHtmlVersion = HttpUtility.UrlEncode(code);
            await _emailSendService.SendEmailAsync(request.Email, "Reset password", $"Your reset password code: {codeHtmlVersion}");

            return Ok(new
            {
                Success = true,
                Message = "Password reset code was sent to your Email. Check your email."
            });
        }

        [HttpPost(ApiRoutes.Identity.ResetPassword)]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordRequest request)
        {
            if (request.Code != null)
            {
                string codeHtmlDecoded = HttpUtility.UrlDecode(request.Code);

                IdentityUser user = await _userManager.FindByEmailAsync(request.Email);

                IdentityResult result = await _userManager.ResetPasswordAsync(user, codeHtmlDecoded, request.NewPassword);

                if (result.Succeeded)
                {
                    return Ok(new
                    {
                        Success = true,
                        ErrorMassage = "The password was changed successfully!"
                    });
                }
                else
                {
                    return BadRequest(new
                    {
                        Success = false,
                        ErrorMassage = "Token is not valid or its lifetime has expired!"
                    });
                }
            }
            else
            {
                return BadRequest(new
                {
                    Success = false,
                    ErrorMassage = "The code was not entered!"
                });
            }
        }



    }
}
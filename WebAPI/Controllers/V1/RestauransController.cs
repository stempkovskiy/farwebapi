﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using WebAPI.Contracts;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Contracts.V1.Responses;
using WebAPI.Domain;
using WebAPI.Services;

namespace WebAPI.Controllers.V1
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RestauransController : ControllerBase
    {

        private readonly IRestauranService _restauranService;

        public RestauransController(IRestauranService restauranService, IHostEnvironment hostEnv)
        {
            _restauranService = restauranService;
        }

        [HttpDelete(ApiRoutes.Restaurants.DeleteImagesRestauran)]
        public async Task<IActionResult> DeleteImagesrestauran([FromBody] IdRequest request)
        {

            var response = await _restauranService.DeleteImagesRstauranAsync(request.Id);
            if (response)
            {
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "Image was successful deleted" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "Image not deleted" }
            });
        }

        [HttpDelete(ApiRoutes.Restaurants.DeleteTimeWork)]
        public async Task<IActionResult> DeleteTimeWork([FromBody] IdRequest request)
        {

            var response = await _restauranService.DeleteTimeWorkAsync(request.Id);
            if (response)
            {
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "Time work was successful delete" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "Time work not deleted" }
            });
        }

        [HttpPost(ApiRoutes.Restaurants.EditInfoRestauran)]
        public async Task<IActionResult> EditInfoRestauran([FromForm]RestauranInfo request)
        {
            var response = await _restauranService.EditInfoRestaurantAsync(request);
            if (response)
            {
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "Info abour restauran was successfull udpade" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "There is no rustoran with this id" }
            });
        }


        [HttpGet(ApiRoutes.Restaurants.GetAllRestaurants)]
        public async Task<IActionResult> GetAllRestaurants()
        {
            var response = await _restauranService.GetAllRestaurantsAsync();
            if (response != null)
            {
                return Ok(new
                {
                    Success = true,
                    response.Count,
                    Restaurans = response
                });
            } else
            {
                return BadRequest(new BaseResponse
                {
                    Success = false,
                    Message = new List<string> { "Error getting restaurans" }
                });
            }
        }

        [HttpDelete(ApiRoutes.Restaurants.DeleteRestauran)]
        public async Task<IActionResult> RemoveRestauran(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var response = await _restauranService.RemoveRestauranAsync(id);
                if (response)
                {
                    return Ok(new BaseResponse {
                        Success = true,
                        Message = new List<string> { "Restauran was succssefull remove" }
                    });
                }
                return BadRequest(new BaseResponse
                {
                    Success = false,
                    Message = new List<string> { "There is no rustoran with this id" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "Id property should not be null or empty" }
            });
        }

        [HttpGet(ApiRoutes.Restaurants.FindRestauran)]
        public async Task<IActionResult> FindRestauran(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var response = await _restauranService.FindRestaurantAsync(id);
                if (response != null)
                {
                    return Ok(new
                    {
                        Success = true,
                        Restauran = response
                    });
                }
                return BadRequest(new BaseResponse { 
                    Success = false,
                    Message = new List<string> { "There is no rustoran with this id" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "Id property should not be null or empty" }
            });
        }

        [HttpPost(ApiRoutes.Restaurants.AddRestauran)]
        public async Task<IActionResult> AddRestauran([FromForm]RestauranInfo request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new BaseResponse
                {
                    Success = false,
                    Message = new List<string> { "Invalid model request" }
                });
            }

       
            var response = await _restauranService.AddRestauranAsync(request);

            if (response)
            {
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "New restauran was successfull added" }
                    
                });
            }
            else
            {
                return BadRequest(new BaseResponse
                {
                    Success = false,
                    Message = new List<string> { "Restauran didn't appened in database" }
                });
            }
        }
    }
}
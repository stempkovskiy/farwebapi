﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Contracts;
using WebAPI.Contracts.V1.Requests;
using WebAPI.Contracts.V1.Requests.UserRequests;
using WebAPI.Contracts.V1.Responses;
using WebAPI.Services;

namespace WebAPI.Controllers.V1
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : ControllerBase
    {

        private readonly IUserService _userService;
        private readonly UserManager<IdentityUser> _userManager;

        public UserController(IUserService userService, UserManager<IdentityUser> userManager)
        {
            _userService = userService;
            _userManager = userManager;
        }

        [HttpDelete(ApiRoutes.User.UnFollow)]
        public async Task<IActionResult> UnFollow([FromBody]IdFollowRequest request)
        {
            var response = await _userService.UnFollowAsync(request.IdFollow, request.IdUser);
            if (response.Success)
            {
                return Ok(new {
                    Success = true,
                    response.Followings,
                    Message = "You unfollow on user" 
                });
            }
            return BadRequest(new {
                Success = false,
                Message = "Failed unfollow on user" 
            });
        }

        [HttpPost(ApiRoutes.User.Follow)]
        public async Task<IActionResult> Follow([FromBody]IdFollowRequest request)
        {
            var response = await _userService.FollowAsync(request.IdFollow, request.IdUser);
            if (response)
            {
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "You follow on user" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "Failed follow on user" }
            });
        }

        [HttpGet(ApiRoutes.User.GetAllUsers)]
        public async Task<IActionResult> GetAllUsers()
        {
            var response = await _userService.GetAllUsersAsync();
            if (response != null)
            {
                return Ok(new UserInfoResponse
                {
                    Success = true,
                    UserInfos = response
                });
            }
            return BadRequest(new UserInfoResponse
            {
                Success = false,
                UserInfos = response
            });
        }

        [HttpGet(ApiRoutes.User.GetUser)]
        public async Task<IActionResult> GetUsers(string id)
        {
            var response = await _userService.GetUsersAsync(id);
            if (response != null)
            {
                return Ok(new UserInfoResponse
                {
                    Success = true,
                    UserInfos = new List<Domain.UserInfo> { response }
                });
            }
            return BadRequest(new UserInfoResponse
            {
                Success = false
            });
        }


        [HttpGet(ApiRoutes.User.GetUserByEmail)]
        public async Task<IActionResult> GetUserByEmail(string email)
        {
            var response = await _userService.GetUserByEmail(email);
            if (response != null)
            {
                return Ok(new UserInfoResponse
                {
                    Success = true,
                    UserInfos = new List<Domain.UserInfo> { response }
                });
            }
            return BadRequest(new UserInfoResponse
            {
                Success = false
            });
        }

        [HttpPost(ApiRoutes.User.LoadAvatar)]
        public async Task<IActionResult> LoadAvatar([FromForm]LoadAvatarRequest request)
        {
            var response = await _userService.LoadAvatarAsync(request.Id, request.Avatar);
            if (response)
            {
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "New Avatar was install" }
                });
            }
            return BadRequest(new BaseResponse { 
                Success = false,
                Message = new List<string> { "Failed install new avatar" }
            });
        }

        [HttpPost(ApiRoutes.User.AddReview)]
        public async Task<IActionResult> AddReview([FromBody]ReviewRequest request)
        {
            var response = await _userService.AddReviewAsync(request);
            if (response)
            {
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "Our review was add" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "Our review not add" }
            });
        }

        [HttpDelete(ApiRoutes.User.DeleteReview)]
        public async Task<IActionResult> DeleteReview([FromBody]IdRequest request)
        {
            var response = await _userService.DeleteReviewAsync(request);
            if (response)
            {
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "Our review was deleted" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "Our review not deleted" }
            });
        }

        [HttpDelete(ApiRoutes.User.DeleteUser)]
        public async Task<IActionResult> DeleteUser([FromBody]DeleteUserRequest request)
        {
            var response = await _userService.DeleteUserAsync(request.Email);
            if (response)
            {
                await _userManager.DeleteAsync(await _userManager.FindByEmailAsync(request.Email));
                return Ok(new BaseResponse
                {
                    Success = true,
                    Message = new List<string> { "User Info was deleted" }
                });
            }
            return BadRequest(new BaseResponse
            {
                Success = false,
                Message = new List<string> { "User info not deleted" }
            });
        }


        [HttpGet(ApiRoutes.User.GetAllNonFollowUsers)]
        public async Task<IActionResult> GetAllNonFollowUsers(string id)
        {
            var response = await _userService.GetAllNonFollowUsers(id);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }
    }
}

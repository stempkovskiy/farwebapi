﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Utils
{
    public static class ImageWorker
    {
        private const string blobNameContainer = "img";
        private const string connectionString = "DefaultEndpointsProtocol=https;AccountName=foodandreviews;AccountKey=CQYjNh8OVO+zErUciRgRDAzQf2QrXpemAbmMI8UFUqlgaErnQWhziWWqI1Czm/X4KD1Dlj5ikM0e6cF0lmp5fQ==;EndpointSuffix=core.windows.net";
        public static bool DeleteImageFromServer(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                File.Delete(path);
                return true;
            }
            return false;
        }

        public static string GetImagePath(IFormFile photo, ImageFolder folder) => $"{folder}/{photo.FileName}";


        public static async Task DeleteImageAzureBlobStoragAsync(string imageName)
        {
            var blobServiceClient = new BlobServiceClient(connectionString);
            var blobContainerClient = blobServiceClient.GetBlobContainerClient(blobNameContainer);
            var blobItems = blobContainerClient.GetBlobsAsync(prefix: "Avatar");
            await foreach (BlobItem blobItem in blobItems)
            {
                BlobClient blobClient = blobContainerClient.GetBlobClient(blobItem.Name);
                if (blobClient.Name.Equals(imageName))
                {
                    await blobClient.DeleteIfExistsAsync();
                }
            }
        }

        public static async Task<string> UploadImageAzureBlobStorageAsync(string imageName, IFormFile imageToUpload)
        {

            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
            CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(blobNameContainer);
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(imageName); 
            cloudBlockBlob.Properties.ContentType = imageToUpload.ContentType;
            await cloudBlockBlob.UploadFromStreamAsync(imageToUpload.OpenReadStream());
            return cloudBlockBlob.StorageUri.PrimaryUri.AbsoluteUri;
        }
    }

    public enum ImageFolder
    {
        Restauran,
        Avatar,
        Menu
    }
}

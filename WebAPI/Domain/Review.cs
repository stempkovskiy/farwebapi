﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Domain
{
    public class Review
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }
        public string Avatar { get; set; }
        public string RestauranPhoto { get; set; }
        public string Name { get; set; }
        public string Resturan { get; set; }
        public string Text { get; set; }
        public double Rating { get; set; }
        public string RestauranId { get; set; }
        public string UserInfoId { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Domain
{
    public class Restauran
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Kitchen { get; set; }
        public double KM { get; set; }
        public string Street { get; set; }
        public double Rating { get; set; }
        public List<TimeWork> TimeWorks {get; set;}
        public List<FilePath> Photos { get; set; }
        public List<FilePath> MenuPhoto { get; set; }
        public List<Review> Reviews { get; set; }
    }
}

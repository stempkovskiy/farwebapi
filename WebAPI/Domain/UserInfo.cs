﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Domain
{
    public class UserInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public FilePath Avatar { get; set; }
        public List<Followers> Followers { get; set; }
        public List<Following> Following { get; set; }
        public List<Review> Reviews { get; set; }
        public string UserId { get; set; }
    }
}

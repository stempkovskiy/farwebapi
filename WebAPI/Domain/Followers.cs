﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Domain
{
    public class Followers
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }
        public string Login { get; set; }
        public int CountReview { get; set; }
        public string Avatar { get; set; }
        public string IdFollow { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace WebAPI.Domain
{
    public class Following
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }
        public string Login { get; set; }
        public int CountReview { get; set;}
        public string Avatar { get; set; }
        public string IdFollowing { get; set; }
        public string UserInfoId { get; set; }

    }
}
